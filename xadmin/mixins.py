from django.forms import Media


class HTMXMixin:
    @property
    def media(self):
        return Media(js=['xadmin/js/xadmin.plugin.htmx.js']) + super().media
