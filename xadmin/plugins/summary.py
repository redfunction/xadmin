from django.db.models import Sum, Count
from django.utils.translation import gettext as _
from django.utils.safestring import mark_safe
from xadmin.sites import site
from xadmin.views import BaseAdminPlugin, ListAdminView

AGGREGATE_METHODS = {
    'sum': Sum,
    'count': Count
}


class SummaryPlugin(BaseAdminPlugin):
    """ Summary section displayed before content in xadmin with responsive grid """

    summary_fields = {}  # Defined in adminx like aggregate_fields

    def init_request(self, *args, **kwargs):
        """ Enable plugin only if summary_fields is defined in the admin class. """
        return bool(getattr(self.admin_view, "summary_fields", {}))

    def _get_summary_row(self, queryset=None):
        """
        Uses the queryset to get dynamic summary values or cached data.
        """
        # Check if summary data is already cached in session
        summary_data = self.admin_view.request.session.get('summary_data')
        if summary_data:
            return summary_data  # Use cached summary

        # No summary data cached, return an empty dictionary or placeholders
        return {}

    def block_results_top(self, context, nodes):
        """ Inserts the summary before the table content in a responsive grid layout """
        # Get the summary data from the session or show placeholders if not calculated yet
        summary_data = self._get_summary_row()

        # If there's no data, show placeholders (***)
        if not summary_data:
            summary_data = {field: "***" for field in self.admin_view.summary_fields.keys()}

        # Ensure values exist to avoid NoneType errors
        summary_html = """
        <style>
            .summary-container {
                display: grid;
                grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
                gap: 15px;
                padding: 15px;
                background: rgb(255, 255, 255);
                border-radius: 8px;
                margin-bottom: 20px;
                text-align: center;
            }
            .summary-box {
                padding: 10px;
                background: white;
                border-radius: 6px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            }
            .summary-box h4 {
                margin: 0;
                font-size: 14px;
                color: #666;
            }
            .summary-box strong {
                font-size: 18px;
                display: block;
                margin-top: 5px;
                font-weight: bold;
                color: #333;
            }
        </style>

        <div class="summary-container">
        """

        for field, agg_method in self.admin_view.summary_fields.items():
            key = f"{field}__{agg_method}"
            value = summary_data.get(key, "***")  # Will show '***' if no summary data exists

            # Format value correctly (avoid commas in numbers)
            formatted_value = f"{value}" if isinstance(value, str) else f"{value:.2f}"

            # Add "EUR" **only** for `sum` fields
            currency_suffix = " EUR" if agg_method == "sum" else ""

            summary_html += f"""
                <div class="summary-box">
                    <h4>{_(field.replace("_", " ").capitalize())} ({_(agg_method.capitalize())})</h4>
                    <strong>{formatted_value}{currency_suffix}</strong>
                </div>
            """

        summary_html += "</div>"

        nodes.append(mark_safe(summary_html))

    def get_media(self, media):
        """ Adds custom CSS for layout """
        return media


# Register the plugin for the Invoice list view
site.register_plugin(SummaryPlugin, ListAdminView)
