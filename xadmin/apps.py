from django.apps import AppConfig
from django.core import checks
from django.utils.translation import gettext_lazy as _
import xadmin


class XAdminConfig(AppConfig):
    """Simple AppConfig which does not do automatic discovery."""

    name = 'xadmin'
    verbose_name = _("Administration")

    def ready(self):
        setattr(xadmin, 'site', xadmin.sites.site)
        xadmin.sites.autodiscover()

