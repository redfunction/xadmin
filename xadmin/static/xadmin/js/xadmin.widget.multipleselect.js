(function ($) {


    // add checkbox render
    $.fn.exform.renders.push(function (f) {
            if ($.fn.selectize) {
                f.find('select:not(.select-search):not(.selectize-off)[multiple=multiple]').selectize({
                    persist: false,
                    delimiter: ',',
                    create: true

                });
                f.find('.select-search').each(function () {
                    var $el = $(this);
                    var preload = $el.hasClass('select-preload');
                    $el.selectize({
                        valueField: 'id',
                        labelField: '__str__',
                        searchField: '__str__',
                        plugins: ['remove_button'],
                        create: false,
                        maxItems: null,
                        closeAfterSelect: true,
                        maxOptions: 10,
                        preload: preload,

                        load: function (query, callback) {
                            if (!preload && !query.length) return callback();
                            $.ajax({
                                url: $el.data('search-url') + $el.data('choices'),
                                dataType: 'json',
                                data: {
                                    '_q_': query,
                                    '_cols': 'id.__str__'
                                },
                                type: 'GET',
                                error: function () {
                                    callback();
                                },
                                success: function (res) {
                                    var objects = null;

                                    if (window.hasOwnProperty("__admin_object_id__")) {
                                        var object_id = window.__admin_object_id__;
                                        objects = [];
                                        $.each(res.objects, function (idx, item) {
                                            if (object_id !== item.id) {
                                                objects.push(item);

                                            }
                                        });
                                    } else {
                                        objects = res.objects;
                                    }
                                    callback(objects);
                                }
                            });
                        },
                        onLoad: function (data) {
                            if (preload) {
                                let selectize = this;
                                data.forEach(function (item) {
                                    if (JSON.parse($el.data('value').replace(/'/g, '"')).includes(item.id)) {
                                        selectize.addItem(item.id);
                                    }
                                });
                            }
                        }

                    });
                });
            }
        }
    )
    ;
})(jQuery);

function unify_options_id_values(filterName) {
    const select = document.querySelector('select[name=' + filterName + ']');
    const options = select.querySelectorAll('option[selected]');

    let values = [];
    options.forEach((option) => {
        values.push(option.value);
        option.remove();
    });

    const newOption = document.createElement('option');
    newOption.value = values.join(',');
    newOption.selected = true;
    newOption.text = 'Selected estates';

    select.add(newOption);
}