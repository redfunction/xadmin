Xadmin |Build Status|
============================================

.. |Build Status| image:: https://travis-ci.org/sshwsfc/xadmin.png?branch=master
   :target: https://travis-ci.org/sshwsfc/xadmin

Drop-in replacement of Django admin comes with lots of goodies, fully extensible with plugin support, pretty UI based on Twitter Bootstrap.

Features
--------

-  Drop-in replacement of Django admin
-  Twitter Bootstrap based UI with theme support
-  Extensible with plugin support
-  Better filter, date range, number range, etc.
-  Built-in data export with xls, csv, xml and json format
-  Dashboard page with widget support
-  In-site bookmarking
-  Full CRUD methods

Screenshots
-----------

.. figure:: https://raw.github.com/sshwsfc/django-xadmin/docs-chinese/docs/images/plugins/action.png
   :alt: Actions
   
.. figure:: https://raw.github.com/sshwsfc/django-xadmin/docs-chinese/docs/images/plugins/filter.png
   :alt: Filter

.. figure:: https://raw.github.com/sshwsfc/django-xadmin/docs-chinese/docs/images/plugins/chart.png
   :alt: Chart

.. figure:: https://raw.github.com/sshwsfc/django-xadmin/docs-chinese/docs/images/plugins/export.png
   :alt: Export Data

.. figure:: https://raw.github.com/sshwsfc/django-xadmin/docs-chinese/docs/images/plugins/editable.png
   :alt: Edit inline

Get Started
-----------

Install
^^^^^^^

Install from bitbucket source:

.. code:: bash

    pip install git+https://git@bitbucket.org/redfunction/xadmin.git

Changelogs
-------------

0.6.0
^^^^^
- Compact with Django1.9.
- Add Clock Picker widget for timepicker.
- Fixed some userface errors.

0.5.0
^^^^^
    
- Update fontawesome to 4.0.3
- Update javascript files to compact fa icons new version
- Update tests for the new instance method of the AdminSite class
- Added demo graphs
- Added quickfilter plugin.
- Adding apps_icons with same logic of apps_label_title.
- Add xlsxwriter for big data export.
- Upgrade reversion models admin list page.
- Fixed reverse many 2 many lookup giving FieldDoesNotExist error.
- Fixed user permission check in inline model.

`Detail`_

.. _Detail: ./changelog.md

Run Demo Locally
----------------
For demo/testing purposes you will also need sample django project
you can download 'demo_app' folder from here https://github.com/marguslaak/django-xadmin

make sure to have DEFAULT_AUTO_FIELD defined in your django application settings or env

.. code:: python

    DEFAULT_AUTO_FIELD='django.db.models.AutoField'

.. code:: bash

    pip install -r requirements.txt
    cd demo_app
    ./manage.py migrate
    ./manage.py runserver

Open http://127.0.0.1:8000 in your browser, the admin user password is ``admin``

Xadmin inlines
--------------
By default inline instance will render all fields in whatever order.
To exclude fields, add them to exclude attribute.

.. code:: python

    exclude = (
        'field_1',
        'field_2',
        ...
        'field_n',
    )
To change ordering of fields, use form_layout attribute

.. code:: python

    form_layout = (
        'field_1',
        'field_2',
        ...
        'field_n',
    )
Any field that is not excluded and not in form_layout is later appended.
For showing fields as read-only or to represent a function as a field, add them to readonly_fields

.. code:: python

    readonly_fields = (
        'field_1',
        'function_name',
    )
Read-only fields are always appended to the end.
Final field layout will be:

- Fields defined and ordered in form_layout
- Fields not defined in form_layout and not excluded
- Read-only fields
